package model.logic;

import java.io.*;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.data_structures.DigrafoConPeso;
import model.data_structures.Edge;
import model.data_structures.Queue;
import model.data_structures.Vertex;
import model.value_objects.*;

public class taller8Manager {

	//__________ CONSTANTES PARA LA CARGA DE LOS ARCHIVOS_____________________
	public static final String DIRECCION_SMALL_JSON 	= "./data/taxi-trips-wrvz-psew-subset-small.json" ;
	public static final String DIRECCION_MEDIUM_JSON 	= "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON		= "./data/Large/taxi-trips-wrvz-psew-subset-0"	  ;
	public static final String COMPLEMENT_DIR			= "-02-2017.json";
	public static final String DIRECCION_TEST_JSON		= "./data/Test.json";

	public static final int CANTIDAD_ARCHIVOS		 = 7; //cantidad de archivos para la carga fragmentada del JSON large
	public static final int MINIMUN_DISTANCE		 = 25; //distancia para la carga de los servicios en el grafo

	private int toName;
	// Grafo
	private DigrafoConPeso principalGraph;
	private DigrafoConPeso toCheckLoadFile;

	//------------------INICIO DE LOS METODOS DE CARGA------------------------
	public void load(String ruta){
		principalGraph =new DigrafoConPeso(6000);
		Gson g = new GsonBuilder().create();
		if(!ruta.equals(DIRECCION_LARGE_JSON)) { cargaSML(g, ruta); toName = (ruta.compareTo(DIRECCION_SMALL_JSON)==0)?1:(ruta.compareTo(DIRECCION_MEDIUM_JSON)==0)?2:4;}
		else {for(int i = 2; i<CANTIDAD_ARCHIVOS+2;i++) cargaSML(g, DIRECCION_LARGE_JSON + i + COMPLEMENT_DIR);toName=3;}}

	private void cargaSML(Gson g, String dirJson){
		try{FileInputStream stream = new FileInputStream(new File(dirJson));
		JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8")); reader.beginArray();

		while (reader.hasNext()) {
			Servicio srvc = g.fromJson(reader, Servicio.class);
			Queue<wrapper> candidatesIn = new Queue<>();
			Queue<wrapper> candidatesFn = new Queue<>();
			if(srvc.hasValidPoints() && srvc.idIn() != "0.0;;;0.0" &&  srvc.idFn() != "0.0;;;0.0") {
				if(principalGraph.isEmpty()) principalGraph.addEdge(srvc.idIn(), srvc.idFn(), srvc);
				else  for (Vertex e : principalGraph.allVertices()) {
					double distance1 = srvc.distanciaCoord(e.info(), srvc.getPickLat(), srvc.getPickLong());
					double distance2 = srvc.distanciaCoord(e.info(), srvc.getDropLat(), srvc.getDropLong());
					if(distance1 <= MINIMUN_DISTANCE) candidatesIn.queue(new wrapper(e, distance1));
					if(distance2 <= MINIMUN_DISTANCE) candidatesFn.queue(new wrapper(e, distance2));
				}

				wrapper temp = new wrapper(), temp2 = new wrapper();

				for (wrapper v : candidatesIn) 
					if(temp.item != null)if(v.distance < temp.distance)temp = v;
					else temp = new wrapper(v.item, v.distance);

				for (wrapper v2 : candidatesFn) 
					if(temp2.item != null)if(v2.distance < temp2.distance)temp2 = v2;
					else temp2 = new wrapper(v2.item, v2.distance);

				if(temp.item == null) 
					principalGraph.addVertice(srvc.idIn());
				if(temp2.item == null) 
					principalGraph.addVertice(srvc.idFn());
				principalGraph.addEdge(srvc.idIn(), srvc.idFn(), srvc);
			}
		}}catch(Exception e){System.err.println("error en la escritura del archivo");e.printStackTrace();}
	}

	public void loadTextArchive() {
		String name = (toName == 1)?"small":(toName == 2)?"medium":(toName == 3)?"large":"test";
		try{
			File f = new File("./docs/Leame.txt");
			FileWriter fw = new FileWriter(f, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			pw.println("_____________________________________________________________");
			pw.println("\n");
			pw.println("el grafo del archivo "+name+" con radio de "+MINIMUN_DISTANCE+" metros contiene:");
			pw.println("	" + principalGraph.vertices() + " vertices");
			pw.println("	" + "que cuentan con "+ principalGraph.edges() + " arcos");
			pw.close();
		}catch(Exception e){System.err.println("error en la escritura del archivo de texto");}
	}

	public void saveGraph2() {
		String name = (toName == 1)?"small":(toName == 2)?"medium":(toName == 3)?"large":"test";
		ArrayList<Edge> a = new ArrayList<>();
		try{
			File f = new File("./docs/"+ name +"Graph"+MINIMUN_DISTANCE+"-"+principalGraph.vertices()+"-.json");
			Gson g = new GsonBuilder().create();
			for (Vertex v : principalGraph.indexedArray()) {
				for (Edge e : v.edges()) {
					a.add(e);
				}
			}
			String json = g.toJson(a);
			FileWriter fw = new FileWriter(f, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			pw.print(json);
			pw.close();
		}catch(Exception e){System.err.println("error en la escritura del archivo JSON");}
	}

	private class wrapper implements Comparable<wrapper>{
		private Vertex item;
		private double distance;
		public wrapper() {this(null,0);}
		public wrapper(Vertex v, double distance) {this.item = v; this.distance = distance;}
		@Override public int compareTo(wrapper o) {	return (this.distance > o.distance)?1:(this.distance == o.distance)?0:-1; }
	}
}
