package model.data_structures;

import model.value_objects.Servicio;

public class Vertex implements Comparable<Vertex>{

	private int index;
	/**
	 * Strign composed by the latitude and the longitude separated by <b><i>;;;</i></b>
	 */
	private String info;
	/**
	 * edges from this vertex to another
	 */
	private RedBlackBST<Edge> adj;
	/**
	 * edges another this vertex to this
	 */
	private RedBlackBST<Edge> edgesIn;

	/**
	 * start a new vertex whit ID = "NaN"
	 */
	public Vertex() {this("NaN", -1);}

	public Vertex(String vertexInfo, int index) {this.info = vertexInfo;
	this.index = index;
	edgesIn = adj = new RedBlackBST<>();}

	/**
	 * 
	 * @return the composed String of the latitude;;;longitude
	 */
	public String info() {return this.info;}

	public void setInfo(String newInfo) {this.info = newInfo;}

	public int index() {return this.index;}

	public int degree() {return this.adj.size();}

	public void setInfoEdge(String to, Servicio weightForEdge) {
		for (Edge e : adj.values())
			if(e.to().equals(to)) { 
				e.addService(weightForEdge); break; }
	}

	public boolean isVertex() {return this.info != "NaN";}

	/**
	 * @param newEdge edge to find a existent edge
	 * @return true if exist an edge between whit the initial conditions <br> <i>initial conditions: the same initial vertex, the same destiny</i>
	 */
	public boolean addEdge(Edge newEdge) {
		Edge temp = adj.get(newEdge);

		if   (temp != null) temp.sumWeight(newEdge);
		else if(temp == null) adj.put(newEdge);
		return temp != null;
	}

	public void addEdgeIn(Edge newEdge) {
		if(edgesIn.contains(newEdge))return;
		edgesIn.put(newEdge);
	}

	public Iterable<Edge> edges(){return this.adj.values();}
	public boolean equals(String info) {return this.info.equals(info);}
	@Override public int compareTo(Vertex v ) {return this.info.compareTo(v.info);}
}
