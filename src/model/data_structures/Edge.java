package model.data_structures;

import model.value_objects.Servicio;

public class Edge implements Comparable<Edge>{

	private double total;
	private int cantServices, lFrom, lTo, srvcWToll, totalSeconds;
	private double totalMiles;
	private String ladoIn, ladoFn;

	public Edge(String Inicio, String Fin, int ladoFrom, int ladoTo) {
		this.ladoIn = Inicio;
		this.ladoFn  = Fin ;
		this.lFrom = ladoFrom;
		this.lTo = ladoTo;
		total = totalMiles = srvcWToll = cantServices = totalSeconds = 0;
	}

	public String from() 	{return ladoIn;	}
	public String to()  	{return ladoFn;	}
	public int ladoFrom() 	{return lFrom;	}
	public int ladoTo() 	{return lTo;	}
	public double weight() 	{return (total + totalMiles + totalSeconds)/(cantServices -srvcWToll);}

	public void addService(Servicio s) {
		cantServices++;
		if(s.payTolls())srvcWToll++;
		total 			+= s.getTripTotal( ) ;
		totalMiles 		+= s.getTripMiles( ) ;
		totalSeconds 	+= s.getTripSeconds();
	}

	public void sumWeight(Edge weightToAdd) {
		this.total 		  += weightToAdd.total		 ;
		this.totalMiles   += weightToAdd.totalMiles	 ;
		this.srvcWToll 	  += weightToAdd.srvcWToll	 ;
		this.cantServices += weightToAdd.cantServices;
		this.totalSeconds += weightToAdd.totalSeconds;
	}

	public boolean equals(Edge e) {return this.compareTo(e) ==0;}
	@Override public int compareTo(Edge e) {return (this.ladoIn +";;;"+this.ladoFn).compareTo((e.ladoIn+";;;"+e.ladoFn));}
	public String toString2(){ return String.format("%d-%d %.2f", ladoIn, ladoFn,weight());}
	@Override public String toString(){
		return "{\"total\":"+ total +",\"cantServices\":"+cantServices+",\"lFrom\":"+lFrom+",\"lTo\":"+lTo+",\"srvcWToll\":"+srvcWToll+",\"totalSeconds\":"+totalSeconds+
				",\"totalMiles\":"+ totalMiles +",\"ladoIn\":\""+ ladoIn +"\",\"ladoFn\":\""+ ladoFn +"\"}";}
}
