package model.value_objects;

/**
 * Representation of a Service object
 */

public class Servicio{	

	//	______________________ATRIBUTOS DE LA CLASE
	private double pickup_centroid_latitude, pickup_centroid_longitude, dropoff_centroid_latitude, dropoff_centroid_longitude;
	private int trip_seconds;
	private double trip_miles, trip_total, tolls;

	//_______________________M�TODOS CONSULTORES
	public int getTripSeconds()  {return trip_seconds;	}
	public double getTripMiles() {return trip_miles;	}
	public double getTripTotal() {return trip_total;	}
	public double tolls() {return this.tolls;}

	public double getPickLat() 	{return this.pickup_centroid_latitude;	}
	public double getPickLong() {return this.pickup_centroid_longitude;	}
	public double getDropLat() 	{return this.dropoff_centroid_latitude;	}
	public double getDropLong() {return this.dropoff_centroid_longitude;}

	//________________________CREACI�N DE REFERENCIAS
	public String idIn() {return this.pickup_centroid_latitude+";;;"+this.pickup_centroid_longitude;}
	public String idFn() {return this.dropoff_centroid_latitude+";;;"+this.dropoff_centroid_longitude;}

	//________________________M�TODOS "VERIFICADORES"
	public boolean payTolls() {return tolls != 0;}
	public boolean hasValidPoints() {return this.pickup_centroid_latitude != 0 && this.pickup_centroid_longitude != 0 &&
			this.dropoff_centroid_latitude != 0 && this.dropoff_centroid_longitude != 0;}

	//_______________________OPERACIONES ALGEBR�ICAS PARA HALLAR LA DISTANCIA ENTRE DOS PUNTOS; EN METROS
	/**
	 * @return Retorna la distancia <b>directa</b> entre el punto 1 y el punto 2 en metros <u>m�todo haversine</u>
	 */
	public double distanciaCoord(double lat1, double long1,double lat2, double long2) {  
		double earthRadius = 6371000;
		lat1 = Math.toRadians(lat1);	lat2 = Math.toRadians(lat2);	long1 = Math.toRadians(long1);	long2 = Math.toRadians(long2);
		double dlon = (long2 - long1),  dlat = (lat2 - lat1), sinlat = Math.sin(dlat / 2), sinlon = Math.sin(dlon / 2);
		double c = 2 * Math.asin (Math.min(1.0, Math.sqrt((sinlat * sinlat) + Math.cos(lat1)*Math.cos(lat2)*(sinlon*sinlon))));
		return 	earthRadius * c;}  
	/**
	 * @param formatedPoint coordinates of the point of the vertex
	 * @param lat2 coordinates of the latitude of the Service
	 * @param long2 coordinates of the longitude of the Service
	 * @return the distance in meters between the service and the reference location
	 */
	public double distanciaCoord(String formatedPoint,double lat2, double long2) {
		String[] la1lo2= formatedPoint.split(";;;");
		double lat1 = Double.parseDouble(la1lo2[0]);
		double long1 = Double.parseDouble(la1lo2[1]);
		return distanciaCoord(lat1, long1, lat2, long2);
	}  
}
