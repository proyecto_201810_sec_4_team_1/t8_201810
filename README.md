﻿* #Estudiante A#
> _**Anderson Barragán Agudelo 201719821**_
* #Estudiante B#
> _**Mario Hurtado 201615007**_

## Creación de métodos por estudiante:##

Anderson Barragán | Mario Hurtado
------------- | -------------
Requerimiento 1: carga| carga
Requerimiento 2: creación del grafo| creación del grafo
Requerimiento 3: carga para os vriados archivos| carga para os vriados archivos
Requerimiento 4: definir JSON para guardado del grafo| definir JSON para guardado del grafo
Requerimiento 5: guardar y recargar el JSON del punto 4| guardar y recargar el JSON del punto 4

* Metodo 1

    Erratas o sugerencias:

        Tomar la latitud y la longitud del punto de inicio del servicio (Lati, Logi)
        ▪ Cuando se realiza la creacion de los grafos a partir del archivo guardado se está utilizando como base el archivo de 25mts
		▪ cuando se carga el archivo, se compara con la carga a partir del JSON de servicios, y escribe true, si corresponde a la creación a partir del anterior
